﻿using UnityEngine;

public class Blade : MonoBehaviour
{
    public GameObject bladeTrailPrefab;
    public float minCuttingVelocity = 0.001f;

    bool isCutting = false;

    Vector2 previousPosition;

    GameObject currentBladeTrail;
    CircleCollider2D circleCollider;

    Rigidbody2D rb;
    Camera cam;

    private void Start()
    {
        cam = Camera.main;
        rb = GetComponent<Rigidbody2D>();
        circleCollider = GetComponent<CircleCollider2D>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartCutting();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            StopCutting();
        }

        if (isCutting)
        {
            UpdateCut();
        }
    }

    void UpdateCut()
    {
        Vector2 newPosition = cam.ScreenToWorldPoint(Input.mousePosition);
        rb.position = newPosition;

        //problema: non vogliamo che il semplice tocco tagli gli oggetti.
        //vogliamo che solamente lo scroll del dito sul display tagli gli oggetti.
        // problema 2: essendo settato a Kinematics il RigidBody2D di Blade, non possiamo
        // prendere la velocità come si fa di solito, perché sarebbe appunto nulla. Allora
        //salviamo e poi confrontiamo due posizioni del taglio per capire se appunto
        //stiamo tagliando oppure no.

        float velocity = (newPosition - previousPosition).magnitude * Time.deltaTime;

        if (velocity > minCuttingVelocity)
        {
            circleCollider.enabled = true;
        }
        else
        {
            circleCollider.enabled = false;
        }

        previousPosition = newPosition;
    }


    void StartCutting()
    {
        isCutting = true;
        currentBladeTrail = Instantiate(bladeTrailPrefab, transform);
        previousPosition = cam.ScreenToWorldPoint(Input.mousePosition);
        circleCollider.enabled = false;
    }

    void StopCutting()
    {
        isCutting = false;
        currentBladeTrail.transform.SetParent(null);

        Destroy(currentBladeTrail, 1f);
        circleCollider.enabled = false;
    }

}
