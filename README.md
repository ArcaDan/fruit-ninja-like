# Fruit Ninja Like
Within the project is the skeleton of the logic behind the famous game "Fruit Ninja". It doesn't want to be a complete replica but just the foundation of the game. Anyone can download, edit and publish this code.

## Build With

|Name|Description|
|----|-------------------|
|C#|The language used|
|Unity|The game framework used|
|Blender|The framework used to make Watermelon|
